import "../scss/estilos.scss";
import "../../public/index.html";
import "angular";

import { Pelicula } from "./pelicula";

let app = angular.module("mis-pelis", []);

app.controller("listadosController", function($scope, $timeout, $http) {
    $scope.tituloPendientes = "Pendientes de ver";
    $scope.tituloVistas = "Vistas";
    $scope.peliculas = [];
    let promesaPeliculas = $http.get("http://localhost:3000/peliculas");
    
    promesaPeliculas.then(function(datos) {
        $scope.peliculas = datos.data;
    })

    $scope.verPelicula = function(idPelicula, puntos) {
        let peliculaACambiar = $scope.peliculas.find( pelicula => pelicula.id == idPelicula);
        peliculaACambiar.valoracion = puntos;
        peliculaACambiar.vista = true;
    }
    $scope.formularioAbierto = false;
    $scope.creaPelicula = function(formulario) {
        let nuevaPelicula = new Pelicula(
            $scope.nuevaPelicula.titulo,
            $scope.nuevaPelicula.director,
            $scope.nuevaPelicula.anyo,
            $scope.nuevaPelicula.cartel
        );
        let promesaNuevaPelicula = $http.post("http://localhost:3000/peliculas", nuevaPelicula);
        promesaNuevaPelicula.then(function(datos) {            
            nuevaPelicula.id = datos.data.id;
            $scope.peliculas.push(nuevaPelicula);            
            console.log($scope.peliculas);
        })
        $scope.formularioAbierto = false;
    }
    $scope.borraPelicula = function(idPelicula) {
        $scope.peliculas = $scope.peliculas.filter( pelicula => pelicula.id != idPelicula );
    }


})

