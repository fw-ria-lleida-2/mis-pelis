export class Pelicula {
    constructor(tit, dir, any, cart) {
        this.titulo = tit;
        this.director = dir;
        this.anyo = any;
        this.cartel = cart;
        this.vista = false;
        this.valoracion = 0;
    }
}
