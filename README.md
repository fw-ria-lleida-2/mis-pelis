#Formación Frameworks RIA multiplataforma - Lleida

##Ejemplo aplicación Mis Pelis
Ejemplo sobre la aplicación de Bootstrap y AngularJS.

###Cómo descargarla
Si quieres usar Git, copia la URL que tienes más arriba, al lado de **HTTPS**, y haz un **git clone** desde tu disco duro.

Si quieres descargarte un zip, ve a la opción **Downloads** del menú izquierdo.

Si es la primera vez que la descargas, o si se ha añadido algún cambio en el archivo package.json, recuerda lanzar **npm install** después de descargarla.